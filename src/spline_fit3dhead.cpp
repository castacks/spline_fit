/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * spline_fit3dhead.cpp
 *
 *  Created on: Feb 27, 2016
 *      Author: Sanjiban Choudhury
 */

#include "spline_fit/spline_fit3dhead.h"
#include "spline_fit/spline_fit1d.h"
#include "exception/exception.h"
#include "math_utils/math_utils.h"
#include <iostream>

namespace ca {
namespace spline_fit {

void UnWrapHeading(std::vector<double> &h_vals) {
  for (std::size_t i = 1; i < h_vals.size(); i++) {
    const double heading_incr (ca::math_utils::angular_math::WrapToPi(h_vals[i] - h_vals[i-1]));
    h_vals[i] = h_vals[i-1] + heading_incr;
  }
}

Spline_3dhead_5ord Fit3DH5OSplineC0(
    const std::vector<double> &times,
    const std::vector<double> &x_vals,
    const std::vector<double> &y_vals,
    const std::vector<double> &z_vals,
    const std::vector<double> &h_vals,
    size_t num_chunks) {
  Spline_3dhead_5ord spline;

  if(num_chunks==0) {
    std::cerr << "Fit3DH5OSplineC0(): num chunks set to 0" << std::endl;
    return spline;
  }
  if (times.size()<2) {
    std::cerr << "Fit3DH5OSplineC0(): only one time" << std::endl;
    return spline;
  }

  const double time_span_traj (times.back() - times.front());
  const double time_interval (time_span_traj/(num_chunks));
  std::pair<std::vector<double>,std::vector<Polynomial<double,5 > > > x_spline (Fit1D5OSplineC0(times, x_vals, time_interval));
  spline.breaks = x_spline.first;
  spline.x_polynomials = x_spline.second;
  spline.y_polynomials = Fit1D5OSplineC0(times, y_vals, time_interval).second;
  spline.z_polynomials = Fit1D5OSplineC0(times, z_vals, time_interval).second;
  std::vector<double> h_vals_unwrapped(h_vals);
  UnWrapHeading(h_vals_unwrapped);
  spline.h_polynomials = Fit1D5OSplineC0(times, h_vals_unwrapped, time_interval).second;
  return spline;
}

Spline_3dhead_5ord Fit3DH5OSplineC1(
    const std::vector<double> &times,
    const std::vector<double> &x_vals,
    const std::vector<double> &y_vals,
    const std::vector<double> &z_vals,
    const std::vector<double> &h_vals,
    const std::vector<double> &vx_vals,
    const std::vector<double> &vy_vals,
    const std::vector<double> &vz_vals,
    const std::vector<double> &vh_vals,
    size_t num_chunks, double weight_lsq) {
  Spline_3dhead_5ord spline;

  // not all combinations... but should be enough
  BOOST_ASSERT_MSG(x_vals.size()==y_vals.size()&&z_vals.size()&&h_vals.size()&&vx_vals.size()&&vy_vals.size()==vz_vals.size()&&vh_vals.size()==times.size(),
                   "Input not consistent");
  if(num_chunks==0) {
    std::cerr << "Fit3DH5OSplineC1(): num chunks set to 0" << std::endl;
    return spline;
  }
  if(times.size()<2) {
    std::cerr <<  "Fit3DH5OSplineC1(): only one time" << std::endl;
    return spline;
  }

  const size_t max_pts_per_chunk (static_cast<size_t>(std::ceil(static_cast<double>(x_vals.size()+num_chunks-1) / static_cast<double>(num_chunks))));
  std::pair<std::vector<double>,std::vector<Polynomial<double,5 > > > x_spline (Fit1D5OSplineC1(times, x_vals, vx_vals,  max_pts_per_chunk, weight_lsq));
  spline.breaks = x_spline.first;
  spline.x_polynomials = x_spline.second;
  spline.y_polynomials = Fit1D5OSplineC1(times, y_vals, vy_vals,  max_pts_per_chunk, weight_lsq).second;
  spline.z_polynomials = Fit1D5OSplineC1(times, z_vals, vz_vals,  max_pts_per_chunk, weight_lsq).second;
  std::vector<double> h_vals_unwrapped(h_vals);
  UnWrapHeading(h_vals_unwrapped);
  spline.h_polynomials = Fit1D5OSplineC1(times, h_vals_unwrapped, vh_vals,  max_pts_per_chunk, weight_lsq).second;
  /* this should be all... */
  return spline;
}

Spline_3dhead_5ord Fit3DHOSplineC2(
    const std::vector<double> &times,
    const std::vector<double> &x_vals,
    const std::vector<double> &y_vals,
    const std::vector<double> &z_vals,
    const std::vector<double> &h_vals,
    const std::vector<double> &vx_vals,
    const std::vector<double> &vy_vals,
    const std::vector<double> &vz_vals,
    const std::vector<double> &vh_vals,
    size_t num_chunks, double weight_lsq, double weight_via_vel, double weight_via_acc) {
  Spline_3dhead_5ord spline;

  // not all combinations... but should be enough
  BOOST_ASSERT_MSG(x_vals.size()==y_vals.size()&&z_vals.size()&&h_vals.size()&&vx_vals.size()&&vy_vals.size()==vz_vals.size()&&vh_vals.size()==times.size(),
                   "Input not consistent");
  if(num_chunks==0) {
    std::cerr << "Fit3DHOSplineC2(): num chunks set to 0" << std::endl;
    return spline;
  }
  if(times.size()<2) {
    std::cerr << "Fit3DHOSplineC2(): only one time" << std::endl;
    return spline;
  }

  const size_t max_pts_per_chunk (static_cast<size_t>(std::ceil(static_cast<double>(x_vals.size()+num_chunks-1) / static_cast<double>(num_chunks))));
  std::pair<std::vector<double>,std::vector<Polynomial<double,5 > > > x_spline (Fit1D5OSplineC2(times, x_vals, vx_vals,  max_pts_per_chunk, weight_lsq, weight_via_vel, weight_via_acc));
  spline.breaks = x_spline.first;
  spline.x_polynomials = x_spline.second;
  spline.y_polynomials = Fit1D5OSplineC2(times, y_vals, vy_vals,  max_pts_per_chunk, weight_lsq, weight_via_vel, weight_via_acc).second;
  spline.z_polynomials = Fit1D5OSplineC2(times, z_vals, vz_vals,  max_pts_per_chunk, weight_lsq, weight_via_vel, weight_via_acc).second;
  std::vector<double> h_vals_unwrapped(h_vals);
  UnWrapHeading(h_vals_unwrapped);
  spline.h_polynomials = Fit1D5OSplineC2(times, h_vals_unwrapped, vh_vals,  max_pts_per_chunk, weight_lsq, weight_via_vel, weight_via_acc).second;
  /* this should be all... */
  return spline;
}


Spline_3dhead_5ord Clone(Spline_3dhead_5ord orig) {
  Spline_3dhead_5ord clone;

  for (int i=0; i<orig.breaks.size(); i++) {
    clone.breaks.push_back(orig.breaks[i]);
    if (i < orig.x_polynomials.size()) {
      Polynomial<double, 5> xp(orig.x_polynomials[i].GetCoefs(), orig.x_polynomials[i].break_l(), orig.x_polynomials[i].break_u());
      Polynomial<double, 5> yp(orig.y_polynomials[i].GetCoefs(), orig.y_polynomials[i].break_l(), orig.y_polynomials[i].break_u());
      Polynomial<double, 5> zp(orig.z_polynomials[i].GetCoefs(), orig.z_polynomials[i].break_l(), orig.z_polynomials[i].break_u());
      Polynomial<double, 5> hp(orig.h_polynomials[i].GetCoefs(), orig.h_polynomials[i].break_l(), orig.h_polynomials[i].break_u());
      clone.x_polynomials.push_back(xp);
      clone.y_polynomials.push_back(yp);
      clone.z_polynomials.push_back(zp);
      clone.h_polynomials.push_back(hp);
    }
  }

  return clone;
}


void Spline_3dhead_5ord::ChopEnd(double chop_time) {
  //BOOST_ASSERT_MSG(GetMinTime()<chop_time && GetMaxTime()>chop_time, "Spline not defined for chopping end time");
  if (chop_time < GetMinTime() || chop_time > GetMaxTime()) {
    std::cerr << "ERROR: [ChopEnd] spline not defined for requested time=" << chop_time << "(min=" << GetMinTime() << " / max=" << GetMaxTime() << ")" << std::endl;
    return;
  }
  const size_t chop_idx (GetPolyIndex(chop_time));
  // First set new final break point
  breaks[chop_idx+1] = chop_time;
  // Erase all the other entries after chop time
  breaks.erase(breaks.begin()+chop_idx+2,breaks.end());
  // Erase also all unnecessary Polynomials, Poly index always one lower than end break index
  x_polynomials.erase(x_polynomials.begin()+chop_idx+1,x_polynomials.end());
  y_polynomials.erase(y_polynomials.begin()+chop_idx+1,y_polynomials.end());
  z_polynomials.erase(z_polynomials.begin()+chop_idx+1,z_polynomials.end());
  h_polynomials.erase(h_polynomials.begin()+chop_idx+1,h_polynomials.end());
  // Done
  return;
}

void Spline_3dhead_5ord::ChopFront(double chop_time) {
  //BOOST_ASSERT_MSG(GetMinTime()<chop_time && GetMaxTime()>chop_time, "Spline not defined for chopping front time");
  if (chop_time < GetMinTime() || chop_time > GetMaxTime()) {
    std::cerr << "ERROR: [ChopFront] spline not defined for requested time=" << chop_time << "(min=" << GetMinTime() << " / max=" << GetMaxTime() << ")" << std::endl;
    return;
  }
  const size_t chop_idx (GetPolyIndex(chop_time));
  // we remove all indices before this chop idx, since we cannot change the break points
  // cannot change value -> otherwise evaluation gives different results
  //breaks[chop_idx] = chop_time;
  // Erase all the other entries bafore chop time
  //std::cout << "Chop front idx: " << chop_idx << std::endl;
  if (chop_idx ==  1) {
    breaks.erase(breaks.begin());
    // Erase also all unnecessary Polynomials, Poly index always one lower than end break index
    x_polynomials.erase(x_polynomials.begin());
    y_polynomials.erase(y_polynomials.begin());
    z_polynomials.erase(z_polynomials.begin());
    h_polynomials.erase(h_polynomials.begin());
  }
  else if (chop_idx > 1) {
    breaks.erase(breaks.begin(), breaks.begin()+chop_idx);
    // Erase also all unnecessary Polynomials, Poly index always one lower than end break index
    x_polynomials.erase(x_polynomials.begin(),x_polynomials.begin()+chop_idx);
    y_polynomials.erase(y_polynomials.begin(),y_polynomials.begin()+chop_idx);
    z_polynomials.erase(z_polynomials.begin(),z_polynomials.begin()+chop_idx);
    h_polynomials.erase(h_polynomials.begin(),h_polynomials.begin()+chop_idx);
  }
  //std::cout << "Min time of Chopped Spline: " << breaks.front() << std::endl;
  // Done
  return;
}

bool Spline_3dhead_5ord::Concatenate(const Spline_3dhead_5ord &end_piece) {
  if(end_piece.Pieces() == 0) return true; // do nothing, nothing to be appended
  if(std::abs(GetMaxTime() - end_piece.GetMinTime()) >= 1e-3) {
    std::cerr << "[ERROR] Spline_3dhead_5ord::Concatenate() - End point of spline does not match start point of other spline" << std::endl;
    return false;
  }
  // concatenate break points, but not the first, since it already exists
  std::vector<double> append_breaks (end_piece.GetBreaks());
  //std::cout << "Size append breaks: " << append_breaks.size() << std::endl;
  breaks.insert(breaks.end(), append_breaks.begin()+1, append_breaks.end());
  //std::cout << "Size total breaks: " << breaks.size() << std::endl;
  for (size_t i(0); i<end_piece.Pieces(); ++i) {
    x_polynomials.push_back(end_piece.GetPoly(i,0));
    y_polynomials.push_back(end_piece.GetPoly(i,1));
    z_polynomials.push_back(end_piece.GetPoly(i,2));
    h_polynomials.push_back(end_piece.GetPoly(i,3));
  }
  return true;
}

Eigen::Vector4d Spline_3dhead_5ord::Evaluate(double time) const{
  Eigen::Vector4d tmp;
  if (time >= GetMinTime() && time <= GetMaxTime()) {
    tmp = EvaluateNoWrap(time);
    tmp[3] = ca::math_utils::angular_math::WrapToPi(tmp[3]);
  }
  return tmp;
}

Eigen::Vector4d Spline_3dhead_5ord::
EvaluateNoWrap(double time) const{
  if (time < GetMinTime() || time > GetMaxTime()) CA_ERROR("[EvaluateNoWrap] spline not defined for requested time");
  size_t idx(GetPolyIndex(time));
  //std::cout << "Idx: " << idx << std::endl;
  return Eigen::Vector4d(
      x_polynomials[idx].Evaluate(time-breaks[idx]),
      y_polynomials[idx].Evaluate(time-breaks[idx]),
      z_polynomials[idx].Evaluate(time-breaks[idx]),
      h_polynomials[idx].Evaluate(time-breaks[idx]));
  //ca::math_utils::angular_math::WrapToPi(h_polynomials[idx].Evaluate(time-breaks[idx])));
}

void Spline_3dhead_5ord::
EvaluateFixedTimeStep(double time_step,
                      std::vector<double> &times,
                      std::vector<double> &x_vals,
                      std::vector<double> &y_vals,
                      std::vector<double> &z_vals,
                      std::vector<double> &h_vals,
                      std::vector<double> &vx_vals,
                      std::vector<double> &vy_vals,
                      std::vector<double> &vz_vals,
                      std::vector<double> &vh_vals) const {
  //TODO check this functions and write unit test!!!

  // should be a bit more efficient thant repeated call of evaluate
  double time_sample (breaks.front());
  // total time span:
  const double time_span (breaks.back() - breaks.front());
  const size_t num_evaluations (static_cast<size_t>(std::ceil(time_span/time_step))); // ceil, since we always will include the last point
  // clear vectors
  times.clear();
  x_vals.clear();
  y_vals.clear();
  z_vals.clear();
  h_vals.clear();
  vx_vals.clear();
  vy_vals.clear();
  vz_vals.clear();
  vh_vals.clear();
  // reserve memory for output
  times.reserve(num_evaluations);
  x_vals.reserve(num_evaluations);
  y_vals.reserve(num_evaluations);
  z_vals.reserve(num_evaluations);
  h_vals.reserve(num_evaluations);
  vx_vals.reserve(num_evaluations);
  vy_vals.reserve(num_evaluations);
  vz_vals.reserve(num_evaluations);
  vh_vals.reserve(num_evaluations);

  size_t poly_idx (0);
  // num of evaluations and time span of each polynomial
  size_t num_eval_poly;
  double time_span_poly;
  while ((time_sample <= breaks.back()) && (poly_idx < Pieces())) {
    time_span_poly = breaks[poly_idx+1] - time_sample;
    if (time_span_poly<= 0) { // if time_step is bigger than time span of polynomial
      poly_idx++;
      if (poly_idx==Pieces()) {
        break;
      }
      continue;
    }
    //TODO check +1
    num_eval_poly = static_cast<size_t>(std::floor((time_span_poly / time_step)+1)); // floor, otherwise we will evaluate outside the defined local polynomial
    Eigen::Vector4d tmp_vec;
    for (size_t i(0); i<num_eval_poly; ++i) {
      tmp_vec = Evaluate(time_sample,poly_idx);
      x_vals.push_back(tmp_vec[0]);
      y_vals.push_back(tmp_vec[1]);
      z_vals.push_back(tmp_vec[2]);
      h_vals.push_back(tmp_vec[3]);

      tmp_vec = EvaluateDot(time_sample,poly_idx);
      vx_vals.push_back(tmp_vec[0]);
      vy_vals.push_back(tmp_vec[1]);
      vz_vals.push_back(tmp_vec[2]);
      vh_vals.push_back(tmp_vec[3]);

      times.push_back(time_sample);
      // increaste sample time
      time_sample += time_step;
    }
    poly_idx++;
    //TODO fix for now, but not really nice
    if (poly_idx==Pieces()) {
      break;
    }
  }
  // Add final point
  Eigen::Vector4d tmp_vec = Evaluate(breaks.back(), x_polynomials.size()-1);
  x_vals.push_back(tmp_vec[0]);
  y_vals.push_back(tmp_vec[1]);
  z_vals.push_back(tmp_vec[2]);
  h_vals.push_back(tmp_vec[3]);

  tmp_vec = EvaluateDot(breaks.back(), x_polynomials.size()-1);
  vx_vals.push_back(tmp_vec[0]);
  vy_vals.push_back(tmp_vec[1]);
  vz_vals.push_back(tmp_vec[2]);
  vh_vals.push_back(tmp_vec[3]);

}

std::pair<std::vector<Eigen::Vector4d>,std::vector<double> > Spline_3dhead_5ord::
EvaluateFixedTimeStep(double time_step) const{
  // should be a bit more efficient thant repeated call of evaluate
  double time_sample (breaks.front());
  // total time span:
  const double time_span (breaks.back() - breaks.front());
  const size_t num_evaluations (static_cast<size_t>(std::ceil(time_span/time_step))); // ceil, since we always will include the last point
  // reserve memory for output
  std::vector<Eigen::Vector4d> result;
  std::vector<double> times;
  result.reserve(num_evaluations);
  times.reserve(num_evaluations);

  size_t poly_idx (0);
  // num of evaluations and time span of each polynomial
  size_t num_eval_poly;
  double time_span_poly;
  while (time_sample <= breaks.back()) {
    time_span_poly = breaks[poly_idx+1] - time_sample;
    if (time_span_poly < 0) { // if time_step is bigger than time span of polynomial
      poly_idx++;
      continue;
    }
    //TODO check +1
    num_eval_poly = static_cast<size_t>(std::floor((time_span_poly / time_step)+1)); // floor, otherwise we will evaluate outside the defined local polynomial
    for (size_t i(0); i<num_eval_poly; ++i) {
      //std::cout << "time_sample: " << time_sample << std::endl;
      //std::cout << "cur break_upper: " << breaks[poly_idx+1] << std::endl;
      result.push_back(Evaluate(time_sample,poly_idx));
      times.push_back(time_sample);
      // increaste sample time
      time_sample += time_step;
    }
    poly_idx++;
    //TODO fix for now, but not really nice
    if (poly_idx==Pieces()) {
      break;
    }
  }
  // Add final point
  Eigen::Vector4d final_pt (Evaluate(breaks.back(), x_polynomials.size()-1));
  //TODO i guess the if is unnecessary
  if (result.back() != final_pt) {
    result.push_back(final_pt);
    times.push_back(breaks.back());
  }
  return std::make_pair(result,times);
}

Eigen::Vector4d Spline_3dhead_5ord::
EvaluateDot(double time) const {
  if (time < GetMinTime() || time > GetMaxTime()) CA_ERROR("[EvaluateDot] spline not defined for requested time");
  size_t idx(GetPolyIndex(time));
  return Eigen::Vector4d(
      x_polynomials[idx].Derivative().Evaluate(time-breaks[idx]),
      y_polynomials[idx].Derivative().Evaluate(time-breaks[idx]),
      z_polynomials[idx].Derivative().Evaluate(time-breaks[idx]),
      ca::math_utils::angular_math::WrapToPi(h_polynomials[idx].Derivative().Evaluate(time-breaks[idx])));
}

Eigen::Vector4d Spline_3dhead_5ord::
EvaluateDDot(double time) const {
  if (time < GetMinTime() || time > GetMaxTime()) CA_ERROR("[EvaluateDDot] spline not defined for requested time");
  size_t idx(GetPolyIndex(time));
  return Eigen::Vector4d(
      x_polynomials[idx].Derivative().Derivative().Evaluate(time-breaks[idx]),
      y_polynomials[idx].Derivative().Derivative().Evaluate(time-breaks[idx]),
      z_polynomials[idx].Derivative().Derivative().Evaluate(time-breaks[idx]),
      ca::math_utils::angular_math::WrapToPi(h_polynomials[idx].Derivative().Derivative().Evaluate(time-breaks[idx])));
}

Eigen::Vector3d Spline_3dhead_5ord::
EvaluateTranslation(double time) const{
  if (time < GetMinTime() || time > GetMaxTime()) CA_ERROR("[EvaluateTranslation] spline not defined for requested time");
  size_t idx(GetPolyIndex(time));
  return Eigen::Vector3d(
      x_polynomials[idx].Evaluate(time-breaks[idx]),
      y_polynomials[idx].Evaluate(time-breaks[idx]),
      z_polynomials[idx].Evaluate(time-breaks[idx]));
}

double Spline_3dhead_5ord::
EvaluateHeading(double time) const{
  if (time < GetMinTime() || time > GetMaxTime()) CA_ERROR("[EvaluateHeading] spline not defined for requested time");
  size_t idx(GetPolyIndex(time));
  return ca::math_utils::angular_math::WrapToPi(h_polynomials[idx].Evaluate(time-breaks[idx]));
}

Polynomial<double,5> Spline_3dhead_5ord::
GetPoly(double time, size_t dim) const {
  if(dim > 3) CA_ERROR("Requested dimension does not exist, available 0:x 1:y 2:z 3:heading");
  if (time < GetMinTime() || time > GetMaxTime()) CA_ERROR("[GetPoly] spline not defined for requested time");
  /* I guess break is not necessary due to return statement */
  switch(dim) {
    case 0:
      return x_polynomials[GetPolyIndex(time)];
    case 1:
      return y_polynomials[GetPolyIndex(time)];
    case 2:
      return z_polynomials[GetPolyIndex(time)];
    case 3:
      return h_polynomials[GetPolyIndex(time)];
  }
}

Polynomial<double,5> Spline_3dhead_5ord::
GetPoly(size_t num, size_t dim) const {
  if(dim>3) CA_ERROR("Requested dimension does not exist, available 0:x 1:y 2:z 3:heading");
  if(num>Pieces()-1) CA_ERROR("Requested number of polynomial not contained in spline");
  /* I guess break is not necessary due to return statement */
  switch(dim) {
    case 0:
      return x_polynomials[num];
    case 1:
      return y_polynomials[num];
    case 2:
      return z_polynomials[num];
    default: //case 3:
      return h_polynomials[num];
  }
}

/*private methods*/
size_t Spline_3dhead_5ord::
GetPolyIndex(double time) const {
  // lower_bound, for the case requested time is last break point
  // problem first break_point... then we habe negative idx ==> special treatment
  if (breaks.size()==0) {
    std::cerr << "[ERROR] Spline_3dhead_5ord::GetPolyIndex() - Spline not initialized/fitted" << std::endl;
    return 0;
  }
  if (time == breaks.front()) {
    return 0;
  }
  //std::cout << "time: " << time << std::endl;
  //std::cout << "break end " << breaks.back() << std::endl;
  //std::cout << "diff: " << breaks.back()-time << std::endl;
  if (std::abs(time-breaks.back()) < 1e-5) {
    //std::cout << "time resquest at the very end" << std::endl;
    return Pieces()-1;
  }
  //std::cout << "Poly Index, Requesting time  " << time << std::endl;
  //std::cout << "Spline has " << breaks.size()-1  << " Pieces" << std::endl;
  ssize_t idx (std::distance(breaks.begin(), std::lower_bound(breaks.begin(),breaks.end(),time))-1);
  // -1 since size()-1 is the idx of the time point till which the polynoms are defined
  //BOOST_ASSERT_MSG(idx>=0 && idx<breaks.size()-1, "Spline is not defined at requested time point");
  if(idx<0 || idx>=(breaks.size()-1)){
    std::cerr << "time=" << time << " / idx=" << (int)idx << " / breaks.size==" << breaks.size() << std::endl;
    std::cerr << "Min.time=" << GetMinTime() << " / Max.time=" << GetMaxTime();
    std::cerr << "[ERROR] Spline_3dhead_5ord::GetPolyIndex() - spline not defined for requested time" << std::endl;
    return 0;
  }
  //std::cout << "Requested Poly Index is  " << idx << std::endl;
  return static_cast<size_t>(idx);
}

Eigen::Vector4d Spline_3dhead_5ord::
Evaluate(double time, size_t idx) const{
  return Eigen::Vector4d(
      x_polynomials[idx].Evaluate(time-breaks[idx]),
      y_polynomials[idx].Evaluate(time-breaks[idx]),
      z_polynomials[idx].Evaluate(time-breaks[idx]),
      ca::math_utils::angular_math::WrapToPi(h_polynomials[idx].Evaluate(time-breaks[idx])));
}

Eigen::Vector4d Spline_3dhead_5ord::
EvaluateDot(double time, size_t idx) const{
  return Eigen::Vector4d(
      x_polynomials[idx].Derivative().Evaluate(time-breaks[idx]),
      y_polynomials[idx].Derivative().Evaluate(time-breaks[idx]),
      z_polynomials[idx].Derivative().Evaluate(time-breaks[idx]),
      ca::math_utils::angular_math::WrapToPi(h_polynomials[idx].Derivative().Evaluate(time-breaks[idx])));
}

boost::array<double,6> Spline_3dhead_5ord::
GetCoefsRev(size_t dim, size_t idx) const {
  typedef std::vector<double>::const_iterator poly_it;
  poly_it b_it; // begin of vector
  poly_it e_it; // end of vector
  /* select correct iterator. Should we add an assert, if dim > 3?*/
  switch (dim) {
    case 0: //X
      b_it = x_polynomials[idx].Begin();
      e_it = x_polynomials[idx].End();
      break;
    case 1: //Y
      b_it = y_polynomials[idx].Begin();
      e_it = y_polynomials[idx].End();
      break;
    case 2: //Z
      b_it = z_polynomials[idx].Begin();
      e_it = z_polynomials[idx].End();
      break;
    default: //H
      b_it = h_polynomials[idx].Begin();
      e_it = h_polynomials[idx].End();
      break;
  }
  /* copy values to std::array*/
  boost::array<double,6> tmp;
  std::copy(b_it,e_it,tmp.rbegin());
  return tmp;
}

void Spline_3dhead_5ord::PrintAsMatlabSpline(std::ostream &out) const {
  out << "breaks = [";
  for (std::size_t i = 0; i < breaks.size(); i++) {
    if(i) out << ", ";
    out << breaks[i] ;
  }
  out << "]" <<std::endl;

  out << "coefs = [";
  for (std::size_t i = 0; i <  x_polynomials.size(); i++) {
    for(size_t j(0); j < x_polynomials[i].Size(); ++j) {
      if(j) out << ", ";
      out << x_polynomials[i].GetCoefsRev()[j];
    }
    out << ";" << std::endl;
    for(size_t j(0); j < y_polynomials[i].Size(); ++j) {
      if(j) out << ", ";
      out << y_polynomials[i].GetCoefsRev()[j];
    }
    out << ";" << std::endl;
    for(size_t j(0); j < z_polynomials[i].Size(); ++j) {
      if(j) out << ", ";
      out << z_polynomials[i].GetCoefsRev()[j];
    }
    out << ";" << std::endl;
    for(size_t j(0); j < h_polynomials[i].Size(); ++j) {
      if(j) out << ", ";
      out << h_polynomials[i].GetCoefsRev()[j];
    }
    out << ";" << std::endl;
  }
  out << "]" <<std::endl;

//  for (std::size_t i = 0; i <  x_polynomials.size(); i++) {
//     out << x_polynomials[i];
//     out << y_polynomials[i];
//     out << z_polynomials[i];
//     out << h_polynomials[i];
//   }
}

}
}


