/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/******************************************************************************
 * Filename: spline_fit1d.h
 * Description:
 *
 * Version: 1.0
 * Created: Jul 18 2014 23:54:52
 * Last modified: May 07 2015 10:40:54 PM
 * Revision: None
 *
 * Author:  althoff
 * e-mail:  althoff@andrew.cmu.edu
 *
 * Revision history
 * Date Author Remarks
 * Jul 18 2014 althoff File created.
 *
 ******************************************************************************/
#ifndef _SPLINEFIT_SPLINEFIT1D_H_
#define _SPLINEFIT_SPLINEFIT1D_H_

#include "polynomials/polynomials.h"

namespace ca{
namespace spline_fit{

typedef std::pair<std::vector<double>,std::vector< Polynomial< double, 5 > > > Spline_1d_5ord;
typedef std::pair<std::vector<double>,std::vector< Polynomial< double, 4 > > > Spline_1d_4ord;
typedef std::pair<std::vector<double>,std::vector< Polynomial< double, 3 > > > Spline_1d_3ord;
typedef std::pair<std::vector<double>,std::vector< Polynomial< double, 2 > > > Spline_1d_2ord;
typedef std::pair<std::vector<double>,std::vector< Polynomial< double, 1 > > > Spline_1d_1ord;

Spline_1d_5ord Fit1D5OSplineC0(
    const std::vector<double> &time,
    const std::vector<double> &val,
    double time_interval);

Spline_1d_5ord Fit1D5OSplineC1(
    const std::vector<double> &time,
    const std::vector<double> &val,
    const std::vector<double> &val_dot,
    size_t chunk_size,
    double weight_lsq);

Spline_1d_5ord Fit1D5OSplineC2(
    const std::vector<double> &time,
    const std::vector<double> &val,
    const std::vector<double> &val_dot,
    size_t chunk_size,
    double weight_lsq,double weigth_via_vel, double weight_via_acc);

double GetIntermediatePoint(const std::vector<double> &time, const std::vector<double> &val, double t);

double LinearInterpolation(double t0, double val0, double t1, double val1, double t_int);

double Evaluate1D5ord(double time, Spline_1d_5ord spline);
double Evaluate1D4ord(double time, Spline_1d_4ord spline);
double Evaluate1D3ord(double time, Spline_1d_3ord spline);
double Evaluate1D2ord(double time, Spline_1d_2ord spline);
double Evaluate1D1ord(double time, Spline_1d_1ord spline);

size_t GetPolyIndex(double time, std::vector<double> breaks);


}//namespace spline_fit
}//namespace ca




#endif
