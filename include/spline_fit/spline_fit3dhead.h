/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
/* Copyright 2015 Sanjiban Choudhury
 * spline_fit3dhead.h
 *
 *  Created on: Feb 27, 2016
 *      Author: Sanjiban Choudhury
 */

#ifndef SPLINE_FIT_INCLUDE_SPLINE_FIT_SPLINE_FIT3DHEAD_H_
#define SPLINE_FIT_INCLUDE_SPLINE_FIT_SPLINE_FIT3DHEAD_H_

#include "spline_fit/spline_fit1d.h"
#include <iostream>
#include <Eigen/Dense>

namespace ca{
namespace spline_fit{

struct Spline_3dhead_5ord {
  std::vector<Polynomial<double,5> > x_polynomials;
  std::vector<Polynomial<double,5> > y_polynomials;
  std::vector<Polynomial<double,5> > z_polynomials;
  std::vector<Polynomial<double,5> > h_polynomials;
  std::vector<double> breaks;

  Eigen::Vector4d Evaluate(double time) const;

  Eigen::Vector4d EvaluateNoWrap(double time) const;

  Eigen::Vector4d EvaluateDot(double time) const;

  Eigen::Vector4d EvaluateDDot(double time) const;

  Eigen::Vector3d EvaluateTranslation(double time) const;

  void ChopEnd(double chop_time);

  void ChopFront(double chop_time);

  bool Concatenate(const Spline_3dhead_5ord &end_piece);

  void EvaluateFixedTimeStep(double time_step, std::vector<double> &times, std::vector<double> &x_vals, std::vector<double> &y_vals,
                             std::vector<double> &z_vals, std::vector<double> &h_vals, std::vector<double> &vx_vals, std::vector<double> &vy_vals,
                             std::vector<double> &vz_vals, std::vector<double> &vh_vals) const;

  std::pair<std::vector<Eigen::Vector4d>,std::vector<double> > EvaluateFixedTimeStep(double time_step) const;

  double EvaluateHeading(double time) const;

  Polynomial<double,5> GetPoly(double time, size_t dim) const;

  Polynomial<double,5> GetPoly(size_t num , size_t dim) const;

  std::vector<double> GetBreaks(void) const { return breaks; };

  size_t Pieces(void) const { return breaks.size()>0 ? breaks.size()-1 : 0; };// should not return negative val if breaks is empty

  double GetMinTime(void) const { return breaks.size()>0 ? breaks.front() : 0.0;/*std::numeric_limits<double>::quiet_NaN();*/};

  double GetMaxTime(void) const { return breaks.size()>0 ? breaks.back()  : 0.0;/*std::numeric_limits<double>::quiet_NaN();*/};

  size_t GetPolyIndex(double time) const;

  boost::array<double,6> GetCoefsRev(size_t dim, size_t idx) const;

  Eigen::Vector4d Evaluate(double time, size_t idx) const;

  Eigen::Vector4d EvaluateDot(double time, size_t idx) const;

  void PrintAsMatlabSpline(std::ostream &out) const;
  
};

void UnWrapHeading(std::vector<double> &h_vals);

Spline_3dhead_5ord Fit3DH5OSplineC0(
    const std::vector<double> &times,
    const std::vector<double> &x_vals,
    const std::vector<double> &y_vals,
    const std::vector<double> &z_vals,
    const std::vector<double> &h_vals,
    size_t num_chunks);

Spline_3dhead_5ord Fit3DH5OSplineC1(
    const std::vector<double> &times,
    const std::vector<double> &x_vals,
    const std::vector<double> &y_vals,
    const std::vector<double> &z_vals,
    const std::vector<double> &h_vals,
    const std::vector<double> &vx_vals,
    const std::vector<double> &vy_vals,
    const std::vector<double> &vz_vals,
    const std::vector<double> &vh_vals,
    size_t num_chunks, double weight_lsq);

Spline_3dhead_5ord Fit3DHOSplineC2(
    const std::vector<double> &times,
    const std::vector<double> &x_vals,
    const std::vector<double> &y_vals,
    const std::vector<double> &z_vals,
    const std::vector<double> &h_vals,
    const std::vector<double> &vx_vals,
    const std::vector<double> &vy_vals,
    const std::vector<double> &vz_vals,
    const std::vector<double> &vh_vals,
    size_t num_chunks, double weight_lsq, double weight_via_vel, double weight_via_acc);

Spline_3dhead_5ord Clone(Spline_3dhead_5ord orig);

}
}

#endif /* SPLINE_FIT_INCLUDE_SPLINE_FIT_SPLINE_FIT3DHEAD_H_ */
