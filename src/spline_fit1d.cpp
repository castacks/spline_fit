/** * @author: AirLab / Field Robotics Center
 *
 * @attention Copyright (C) 2016
 * @attention Carnegie Mellon University
 * @attention All rights reserved
 *
 * @attention LIMITED RIGHTS:
 * @attention The US Government is granted Limited Rights to this Data.
 *            Use, duplication, or disclosure is subject to the
 *            restrictions as stated in Agreement AFS12-1642.
 */
#include <iostream>

#include "spline_fit/spline_fit1d.h"
//#include "ca_common/math.h"
#include "exception/exception.h"
#include  <tgmath.h> // for hypot()
#include "polynomials/polynomial_fit.h"
using namespace ca;
using namespace spline_fit;

namespace pf = ca::polynomial_fit;
namespace ca{
namespace spline_fit{

Spline_1d_5ord Fit1D5OSplineC0(const std::vector<double> &times, const std::vector<double> &vals, double time_interval) {
  /* input sizes must be same length*/
  //BOOST_ASSERT_MSG(times.size() && vals.size(), "Expected same length of times and vals");
  if (times.size() != vals.size()) CA_ERROR("Expected same length of times and vals");
  std::vector<double> breaks;
  std::vector<Polynomial<double,5> > polynomials;
    if(times.size() == 0) {
    // return empty vectors
    return std::make_pair(breaks,polynomials);
  }

  double time_begin;
  double time_end;
    //std::cout << "SplineFit1d received min time:  " << times.front() << std::endl;
    //std::cout << "SplineFit1d received max time:  " << times.back() << std::endl;
  std::vector<double> chunk_vals(6);//TODO make a const in header for 6
  //for (size_t i(0); (i+1)*time_interval <= times.back(); ++i) {

    for (size_t i(0); times.front()+(i+1)*time_interval <= times.back()+1e-9; ++i) {
    //for (size_t i(0); times.front()+(i+1)*time_interval <= times.back(); ++i) {
      time_begin = times.front() +    i *time_interval;
    time_end   = times.front() + (1+i)*time_interval;
    breaks.push_back(time_begin);
//    std::cout << "time begin: " << time_begin << std::endl;
//    std::cout << "time end: " << time_end << std::endl;
//    std::cout << "time interval: " << time_interval << std::endl;
    for (size_t j(0); j<chunk_vals.size(); ++j) {
//      std::cout << "j: " << j << std::endl;
//      std::cout << "interpolated time: " << time_begin+j*(time_interval/5.0) << std::endl;
      chunk_vals[j] = GetIntermediatePoint(times, vals, time_begin+j*(time_interval/5.0));
    }
    Polynomial<double,5> poly = pf::Fit5DPolyC0(time_begin, time_end, chunk_vals);
    poly.ResetBreakOffset(); // SPLINE maintaines breaks
    polynomials.push_back(poly);
  }
  /*don't forget last break point*/
  breaks.push_back(time_end);
  return std::make_pair(breaks,polynomials);
}


/*SplineFit1d*/
Spline_1d_5ord Fit1D5OSplineC1(const std::vector<double> &time, const std::vector<double> &val, const std::vector<double> &val_dot, size_t chunk_size, double weight_lsq) {
	
	/* input sizes must be same length*/
	BOOST_ASSERT_MSG(time.size() == val.size() && val.size() == val_dot.size(),"Check inputs: Same length of time pos and vel expected!");

	std::vector<double> breaks;
	std::vector<Polynomial<double,5> > polynomials;
	size_t size (time.size());
	size_t i(0);
	if(size == 0) {
		// return empty vectors
		return std::make_pair(breaks,polynomials);
	}

	/*is traj long enough for more than 1 chunk*/
	//std::cout << "SplineFit1d.FitSpline, size: " << size << " chunk_size: " << chunk_size << std::endl;
	if (size > chunk_size) {
		for (;i<size-chunk_size+1; i+=chunk_size-1) {//-1 since the next chunk start point is the final point of last chunk
			/* setup all chunks */
			const double t0(*(time.begin()+i));
			const double tT(*(time.begin()+i+chunk_size-1));
			const double x0(*(val.begin()+i));
			const double xT(*(val.begin()+i+chunk_size-1));
			const double x0_dot(*(val_dot.begin()+i));
			const double xT_dot(*(val_dot.begin()+i+chunk_size-1));
			std::vector<double> time_lsq (time.begin()+i,time.begin()+i+chunk_size);
			/*remove t0 for all time points*/
			std::for_each(time_lsq.begin(), time_lsq.end(), [t0](double& d) { d-=t0;});
			Polynomial<double,5> poly = pf::Fit5DPolyC1(t0, x0, x0_dot, tT, xT, xT_dot,
			                                            time_lsq,
			                                            std::vector<double>(val.begin()+i,val.begin()+i+chunk_size),
			                                            weight_lsq);
			poly.ResetBreakOffset(); //SPLINE maintains breaks
			polynomials.push_back(poly);
			breaks.push_back(t0);
		}
	}

	size_t num_left (size-1-i);// TODO, I guess -1 is wrong
	// if we still have a part of a chunk left, handle it
    	if (num_left <= 1) {
		// do nothing, ignore last point
    	}
	else { // num_left>1 
		/* setup all chunks */
		const double t0(*(time.begin()+i));
		const double tT(*(time.end()-1));
		const double x0(*(val.begin()+i));
		const double xT(*(val.end()-1));
		const double x0_dot(*(val_dot.begin()+i));
		const double xT_dot(*(val_dot.end()-1));
		std::vector<double> time_lsq (time.begin()+i,time.end()-1);
		/*remove t0 for all time points*/
		std::for_each(time_lsq.begin(), time_lsq.end(), [t0](double& d) { d-=t0;});
		Polynomial<double,5> poly = pf::Fit5DPolyC1(t0, x0, x0_dot, tT, xT, xT_dot,
		                                            time_lsq,
		                                            std::vector<double>(val.begin()+i,val.end()-1),
		                                            weight_lsq);
		poly.ResetBreakOffset(); //SPLINE maintans breaks
		polynomials.push_back(poly);
		breaks.push_back(t0);
	}
	/*don't forget last break point*/
	breaks.push_back(*(time.end()-1));
	return std::make_pair(breaks,polynomials);
}

/* Fit splien with vel and pos information, and ensure a C2 spline by ensuring acc constraints fromt the previous spline segment*/
Spline_1d_5ord Fit1D5OSplineC2(const std::vector<double> &time, const std::vector<double> &val, const std::vector<double> &val_dot, size_t chunk_size, double weight_lsq, double weight_via_vel, double weight_via_acc) {
	
	/* input sizes must be same length*/
	BOOST_ASSERT_MSG(time.size() == val.size() && val.size() == val_dot.size(),"Check inputs: Same length of time pos and vel expected!");

	std::vector<double> breaks;
	std::vector<Polynomial<double,5> > polynomials;
	size_t size (time.size());
	size_t i(0);
	if(size == 0) {
		// return empty vectors
		return std::make_pair(breaks,polynomials);
	}
    // compute discrete derivative of vel information
    std::vector<double> val_ddot; val_ddot.reserve(val_dot.size());
    for(size_t i(0); i<val_dot.size()-1;++i) {
        val_ddot.push_back((val_dot[i+1] - val_dot[i]) / (time[i+1] - time[i]));
    }
    val_ddot.push_back(*(val_ddot.end()-2));//last value same as value before

    //std::cout << "val_ddot: " << std::endl;
    //for (auto i:val_ddot) {
    //    std::cout << i << ", ";
    //}
    //std::cout << std::endl;

    // first fit without acc constraints
	const double t0(*(time.begin()+i));
	const double tT(*(time.begin()+i+chunk_size-1));
	const double x0(*(val.begin()+i));
	const double xT(*(val.begin()+i+chunk_size-1));
	const double x0_dot(*(val_dot.begin()+i));
	const double xT_dot(*(val_dot.begin()+i+chunk_size-1));
    const double x0_ddot( (*(val_dot.begin()+1) - *val_dot.begin()) / (*(time.begin()+1) - *time.begin()) );// approximate first acc value
	//std::cout << "FitC2Spline, x0_ddot: " << x0_ddot << std::endl;
    std::vector<double> time_lsq (time.begin()+i+1,time.begin()+i+chunk_size-1);
	/*remove t0 for all time points*/
	std::for_each(time_lsq.begin(), time_lsq.end(), [t0](double& d) { d-=t0;});
	Polynomial<double,5> poly = pf::Fit5DPolyC2(t0,x0,x0_dot,x0_ddot,tT,xT,xT_dot, time_lsq,  std::vector<double>(val.begin()+i+1,val.begin()+i+chunk_size-1),
	        std::vector<double>(val_dot.begin()+i+1,val_dot.begin()+i+chunk_size-1),std::vector<double>(val_ddot.begin()+i+1,val_ddot.begin()+i+chunk_size-1),weight_lsq, weight_via_vel,weight_via_acc);
	poly.ResetBreakOffset();
	polynomials.push_back(poly);
	breaks.push_back(t0);
    i+=chunk_size-1;
    // Next fits will ensure C2 with acc constraint
	/*is traj long enough for more than 1 chunk*/
	//std::cout << "SplineFit1d.FitSpline, size: " << size << " chunk_size: " << chunk_size << std::endl;
	if (size > chunk_size) {
		for (;i<size-chunk_size+1; i+=chunk_size-1) {//-1 since the next chunk start point is the final point of last chunk
			/* setup all chunks */
			const double t0(*(time.begin()+i));
			const double tT(*(time.begin()+i+chunk_size-1));
			const double x0(*(val.begin()+i));
			const double xT(*(val.begin()+i+chunk_size-1));
			const double x0_dot(*(val_dot.begin()+i));
			const double xT_dot(*(val_dot.begin()+i+chunk_size-1));
            const double x0_ddot(polynomials.back().Derivative().Derivative().Evaluate(t0-breaks.back()));// get last acc value of former segment
			std::vector<double> time_lsq (time.begin()+i+1,time.begin()+i+chunk_size-1);
            /*remove t0 for all time points*/
			std::for_each(time_lsq.begin(), time_lsq.end(), [t0](double& d) { d-=t0;});
			Polynomial<double,5> poly = pf::Fit5DPolyC2(t0,x0,x0_dot,x0_ddot,tT,xT,xT_dot,
			                                            time_lsq,
			                                                    std::vector<double>(val.begin()+i+1,val.begin()+i+chunk_size-1),
			                                                    std::vector<double>(val_dot.begin()+i+1,val_dot.begin()+i+chunk_size-1),
			                                                    std::vector<double>(val_ddot.begin()+i+1,val_ddot.begin()+i+chunk_size-1),weight_lsq, weight_via_vel, weight_via_acc);
			poly.ResetBreakOffset();
			polynomials.push_back(poly);
			breaks.push_back(t0);
//            std::cout << "Data for splinec2fit: " << std::endl;
//            std::cout << "weight_lsq: " << weight_lsq << std::endl;
//            std::cout << "weight_via_vel: " << weight_via_vel << std::endl;
//            std::cout << "weight_via_acc: " << weight_via_acc << std::endl;
//            std::cout << "t0: " << t0 << std::endl;
//            std::cout << "tT: " << tT << std::endl;
//            std::cout << "x0: " << x0 << std::endl;
//            std::cout << "xT: " << xT << std::endl;
//            std::cout << "x0_dot: " << x0_dot << std::endl;
//            std::cout << "xT_dot: " << xT_dot << std::endl;
//            std::cout << "x0_ddot: " << x0_ddot << std::endl;
//            std::cout << "time_lsq: ";
//            for (auto i : time_lsq)
//                std::cout << i << ", ";
//            std::cout << "\n";
//            std::cout << "x_via: ";
//            std::vector<double> x_via = std::vector<double>(val.begin()+i+1,val.begin()+i+chunk_size-1);
//            for (auto i : x_via)
//                std::cout << i << ", ";
//            std::cout << "\n";
//            std::cout << "x_via_dot: ";
//            std::vector<double> x_via_dot = std::vector<double>(val_dot.begin()+i+1,val_dot.begin()+i+chunk_size-1);
//            for (auto i : x_via_dot)
//                std::cout << i << ", ";
//            std::cout << "\n";
//            std::cout << "x_via_ddot: ";
//            std::vector<double> x_via_ddot = std::vector<double>(val_ddot.begin()+i+1,val_ddot.begin()+i+chunk_size-1);
//            for (auto i : x_via_ddot)
//                std::cout << i << ", ";
//            std::cout << "\n";
//            std::cout << "Fitted polynomial: " << polynomials.back() << std::endl;
		}
	}

	size_t num_left (size-1-i);// TODO, I guess -1 is wrong
	// if we still have a part of a chunk left, handle it
    if (num_left <= 1) {
		// do nothing, ignore last point
    }
	else { // num_left>1 
		/* setup all chunks */
		const double t0(*(time.begin()+i));
		const double tT(*(time.end()-1));
		const double x0(*(val.begin()+i));
		const double xT(*(val.end()-1));
		const double x0_dot(*(val_dot.begin()+i));
		const double xT_dot(*(val_dot.end()-1));
        const double x0_ddot(polynomials.back().Derivative().Derivative().Evaluate(t0-breaks.back()));// get last acc value of former segment
		std::vector<double> time_lsq (time.begin()+i+1,time.end()-1);
		/*remove t0 for all time points*/
		std::for_each(time_lsq.begin(), time_lsq.end(), [t0](double& d) { d-=t0;});
		Polynomial<double,5> poly = pf::Fit5DPolyC2(t0,x0,x0_dot,x0_ddot,tT,xT,xT_dot,
		                                            time_lsq,std::vector<double>(val.begin()+i+1,val.end()-1),
		                                                  std::vector<double>(val_dot.begin()+i+1,val_dot.end()-1),
		                                                  std::vector<double>(val_ddot.begin()+i+1,val_ddot.end()-1),
		                                                  weight_lsq, weight_via_vel, weight_via_acc);
		poly.ResetBreakOffset();
		polynomials.push_back(poly);
		breaks.push_back(t0);
	}
	/*don't forget last break point*/
	breaks.push_back(time.back());
    //std::cout << "FitC2Spline, polynomials: " << std::endl;
    //for(auto i : polynomials) 
    //    std::cout << i << std::endl;
	return std::make_pair(breaks,polynomials);
}


double GetIntermediatePoint(const std::vector<double> &times, const std::vector<double> &vals, double t) {
	/*assumed times is sorted! */
	//BOOST_ASSERT_MSG(times.front() <= t && times.back()>= t, "Requested time point not defined in trajectory times");
	//std::cout << "times front: " << times.front() << std::endl;
	//std::cout << "times back: " << times.back() << std::endl;
	//std::cout << "t: " << t  << std::endl;
	t=std::min(t,times.back());
	//if (t - times.back() < 1e-5) {
	//	std::cout << " bounded t to times.back()"  << std::endl;
	//	t=times.back();
	//}
	if ( t < times.front() || t > times.back()) CA_ERROR("Requested time point not defined in trajectory times");
	std::vector<double>::const_iterator low_bound (std::lower_bound(times.cbegin(),times.cend(), t));
	
	size_t idx_low_bound (std::distance(times.cbegin(), low_bound)); 	
	if (*low_bound == t) {
		return vals[idx_low_bound]; 
	} 
	else { // low_bound must be greater than t
		return LinearInterpolation(times[idx_low_bound-1], vals[idx_low_bound-1], times[idx_low_bound], vals[idx_low_bound], t);
	}
}

double LinearInterpolation(double t0, double val0, double t1, double val1, double t_int) {
	double prog ((t_int-t0)/(t1-t0));
	//std::cout << "t0: " << t0 << ", val0: " << val0 << ", t1: " << t1 << ", val1" << val1 << ", t_int: " << t_int << std::endl;
    double tmp_time ((1-prog)*t0 + prog * t1);
    return ((1-prog) * val0 + prog * val1);
}

// Returns the index of the polynomial that this time corresponds to
size_t GetPolyIndex(double time, std::vector<double> breaks, int num_polys) {

	if (breaks.size() == 0) {CA_ERROR("Spline not initialized/fitted");}

	if (std::abs(time - breaks.front()) < 1e-5) {
		return 0;
	}
	//std::cout << "time: " << time << std::endl;
	//std::cout << "break end " << breaks.back() << std::endl;
	//std::cout << "diff: " << breaks.back()-time << std::endl;
	if (std::abs(time-breaks.back()) < 1e-5) {
		//std::cout << "time resquest at the very end" << std::endl;
		return num_polys-1;
	}
	//std::cout << "Poly Index, Requesting time  " << time << std::endl;
	//std::cout << "Spline has " << breaks.size()-1  << " Pieces" << std::endl;
	ssize_t idx (std::distance(breaks.begin(), std::lower_bound(breaks.begin(),breaks.end(),time))-1);
	// -1 since size()-1 is the idx of the time point till which the polynoms are defined
	//BOOST_ASSERT_MSG(idx>=0 && idx<breaks.size()-1, "Spline is not defined at requested time point");
	if(idx<0 || idx>=(breaks.size()-1)){
		std::cerr << "time=" << time << " / idx=" << (int)idx << " / breaks.size==" << breaks.size() << std::endl;
		std::cerr << "Min.time=" << breaks.front() << " / Max.time=" << breaks.back();
		CA_ERROR( "[GetPolyIndex] spline not defined for requested time");
	}
	//std::cout << "Requested Poly Index is  " << idx << std::endl;
	return static_cast<size_t>(idx);
}

double Evaluate1D5ord(double time, Spline_1d_5ord spline) {
	std::vector<double> breaks = spline.first;
	std::vector< Polynomial< double, 5 > > polys = spline.second;
	if (time < breaks.front() || time > breaks.back())
		CA_ERROR("[EvaluateNoWrap] spline not defined for requested time");

	size_t idx(GetPolyIndex(time, breaks, polys.size()));

	return polys[idx].Evaluate(time-breaks[idx]);
}

double Evaluate1D4ord(double time, Spline_1d_4ord spline) {
	std::vector<double> breaks = spline.first;
	std::vector< Polynomial< double, 4 > > polys = spline.second;
	if (time < breaks.front() || time > breaks.back())
		CA_ERROR("[EvaluateNoWrap] spline not defined for requested time");

	size_t idx(GetPolyIndex(time, breaks, polys.size()));

	return polys[idx].Evaluate(time-breaks[idx]);
}

double Evaluate1D3ord(double time, Spline_1d_3ord spline) {
	std::vector<double> breaks = spline.first;
	std::vector< Polynomial< double, 3 > > polys = spline.second;
	if (time < breaks.front() || time > breaks.back())
		CA_ERROR("[EvaluateNoWrap] spline not defined for requested time");

	size_t idx(GetPolyIndex(time, breaks, polys.size()));

	return polys[idx].Evaluate(time-breaks[idx]);
}

double Evaluate1D2ord(double time, Spline_1d_2ord spline) {
	std::vector<double> breaks = spline.first;
	std::vector< Polynomial< double, 2 > > polys = spline.second;
	if (time < breaks.front() || time > breaks.back())
		CA_ERROR("[EvaluateNoWrap] spline not defined for requested time");

	size_t idx(GetPolyIndex(time, breaks, polys.size()));

	return polys[idx].Evaluate(time-breaks[idx]);
}

double Evaluate1D1ord(double time, Spline_1d_1ord spline) {
	std::vector<double> breaks = spline.first;
	std::vector< Polynomial< double, 1 > > polys = spline.second;
	if (time < breaks.front() || time > breaks.back())
		CA_ERROR("[EvaluateNoWrap] spline not defined for requested time");

	size_t idx(GetPolyIndex(time, breaks, polys.size()));

	return polys[idx].Evaluate(time-breaks[idx]);
}

} // ca
} // spline_fit
